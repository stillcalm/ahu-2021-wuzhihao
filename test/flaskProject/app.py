from flask import Flask, request
import pymysql

app = Flask(__name__)

conn = pymysql.connect(
    host='localhost',
    user='root',
    password='countpassword',
    port=3306,
    db='AHU_2021_TEST',
    charset='utf8'
)


def add_data(operand1, operator, operand2, result):
    # 定义游标
    cursor = conn.cursor()
    # 定义将要执行的SQL语句
    sql = "insert into calc_record(operand1, operator, operand2, result) values(%s, %s, %s, %s);"
    # 拼接并执行SQL语句
    cursor.execute(sql, [operand1, operator, operand2, result])
    # 提交操作
    conn.commit()
    cursor.close()
    conn.close()


@app.route('/')
# 获得url中的参数a, b
def get_num():
    x1 = request.args.get('a')
    x2 = request.args.get('b')
    return x1, x2


@app.route('/add')
def add():
    num = get_num()
    # 将获取的字符转化为数字
    a = int(num[0], base=10)
    b = int(num[1], base=10)
    # 求和
    r = a + b
    result = str(r) # 转化为字符串，输入
    add_data(num[0], "+", num[1], result)
    return result


@app.route('/sub')
def sub():
    num = get_num()
    a = int(num[0], base=10)
    b = int(num[1], base=10)
    # 求差
    r = a - b
    result = str(r)
    add_data(num[0], "-", num[1], result)
    return result


@app.route('/multiply')
def multiply():
    num = get_num()
    a = int(num[0], base=10)
    b = int(num[1], base=10)
    # 求积
    r = a * b
    result = str(r)
    add_data(num[0], "*", num[1], result)
    return result


@app.route('/division')
def division():
    num = get_num()
    a = int(num[0], base=10)
    b = int(num[1], base=10)
    # 求商
    r = a / b
    result = str(r)
    add_data(num[0], "/", num[1], result)
    return result


if __name__ == '__main__':
    app.run()
